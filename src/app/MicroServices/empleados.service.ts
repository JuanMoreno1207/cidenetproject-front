import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Empleado } from '../Entities/Empleado';

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {

  constructor(private http:HttpClient) { }

  Endpoint:String=environment.EndPoint;
  
  ConsultaEmpleados(){    
    return this.http.get<Empleado[]>(this.Endpoint+'/ConsultaEmpleados');
  }

  GuardarEmpleado(empleado: Empleado){
    return this.http.post(this.Endpoint+'/GuardaEmpleado', empleado);
  }

  EliminaEmpleado(id: number){
    return this.http.delete(this.Endpoint+'/EliminarEmpleado/'+id);
  }

  ConsultaEmpleadoCorreo(correo: string){
    return this.http.get<Empleado[]>(this.Endpoint+'/ConsultaPorCorreo/'+correo);
  }

  ConsultaIdMax(){
    return this.http.get<number>(this.Endpoint+'/ConsultaIdMax')
  }
}
