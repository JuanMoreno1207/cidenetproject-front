import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaEmpleadosComponent } from './Components/lista-empleados/lista-empleados.component';
import { RegistroEmpleadosComponent } from './Components/registro-empleados/registro-empleados.component';

const routes: Routes = [
  {
    path:'',
    component: ListaEmpleadosComponent,
    pathMatch: 'full'
  },
  {
    path:'consulta-empleado',
    component: ListaEmpleadosComponent
  },
  {
    path:'registro-empleado',
    component: RegistroEmpleadosComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
