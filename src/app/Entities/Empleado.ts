export class Empleado{    
    id:number;
    correo:string;
    otrosNombres:string;
    pais:string;
    primerApel:string;
    primerNombre:string;
}