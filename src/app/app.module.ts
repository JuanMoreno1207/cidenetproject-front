import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaEmpleadosComponent } from './Components/lista-empleados/lista-empleados.component';
import { MenuSuperiorComponent } from './Components/menu-superior/menu-superior.component';
import { HttpClientModule } from '@angular/common/http';
import { RegistroEmpleadosComponent } from './Components/registro-empleados/registro-empleados.component'
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoggerModule, NgxLoggerLevel } from 'ngx-logger';
import { environment } from 'src/environments/environment';



@NgModule({
  declarations: [
    AppComponent,
    ListaEmpleadosComponent,
    MenuSuperiorComponent,
    RegistroEmpleadosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    NgbModule,
    LoggerModule.forRoot({
      serverLoggingUrl: environment.EndPoint+'/logs',
      level: environment.logLevel,
      serverLogLevel: environment.serverLogLevel,
      disableConsoleLogging: false
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
