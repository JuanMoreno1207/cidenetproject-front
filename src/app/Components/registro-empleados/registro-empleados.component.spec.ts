import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RegistroEmpleadosComponent } from './registro-empleados.component';
import { FormsModule } from '@angular/forms';
import { LoggerModule } from 'ngx-logger';
import { environment } from 'src/environments/environment';

describe('RegistroEmpleadosComponent', () => {
  let component: RegistroEmpleadosComponent;
  let fixture: ComponentFixture<RegistroEmpleadosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule,
        FormsModule,
        LoggerModule.forRoot({
          serverLoggingUrl: environment.EndPoint+'/logs',
          level: environment.logLevel,
          serverLogLevel: environment.serverLogLevel,
          disableConsoleLogging: false
        })
      ],
      providers: [
        
      ],
      declarations: [RegistroEmpleadosComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistroEmpleadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
