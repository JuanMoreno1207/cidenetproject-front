import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';
import { Empleado } from 'src/app/Entities/Empleado';
import { EmpleadosService } from 'src/app/MicroServices/empleados.service';

@Component({
  selector: 'app-registro-empleados',
  templateUrl: './registro-empleados.component.html',
  styleUrls: ['./registro-empleados.component.css']
})
export class RegistroEmpleadosComponent implements OnInit {

  empleado = new Empleado();

  constructor(private EmpleadoServices: EmpleadosService, private router: Router, private logger: NGXLogger) { }

  ngOnInit(): void {    
  }

  GuardaEmpleado() {
    this.AsignaCorreoEmpleado('1');    
    this.EmpleadoServices.ConsultaEmpleadoCorreo(this.empleado.correo).subscribe({
      next: (ResulCorreo) => {
        if (ResulCorreo.length > 0) {
          this.EmpleadoServices.ConsultaIdMax().subscribe({
            next: (Result) => {
              this.empleado.id = Result;
              this.AsignaCorreoEmpleado('2');
              this.EmpleadoServices.GuardarEmpleado(this.empleado).subscribe({
                next: (ResultReproceso) => {
                  this.router.navigate(['/consulta-empleado']);
                },
                error: (Erroract) => {
                  this.logger.error({
                    message: Erroract.message,
                    linea: "34",
                    componente: "registro-empleados.component.ts",
                  });
                }
              })
            },
            error:(ErrorCons)=>{
              this.logger.error({
                message: ErrorCons.message,
                linea: "43",
                componente: "registro-empleados.component.ts",
              });
            }
          })
        }
        else {
          this.EmpleadoServices.GuardarEmpleado(this.empleado).subscribe({
            next: (ResultReproceso) => {
              this.router.navigate(['/consulta-empleado']);
            },
            error: (Erroract) => {
              this.logger.error({
                message: Erroract.message,
                linea: "46",
                componente: "registro-empleados.component.ts",
              });
            }
          })
        }
      },
      error: (Error) => {
        this.logger.error({
          message: Error.message,
          linea: "51",
          componente: "registro-empleados.component.ts",
        });
      }
    })
  }

  AsignaCorreoEmpleado(bandera: string) {
    if (bandera == '1') {
      this.empleado.correo = this.empleado.primerNombre.toLowerCase() + '.' + this.empleado.primerApel.replace(/\s+/g, '').toLowerCase();
    }
    else {
      this.empleado.correo = this.empleado.primerNombre.toLowerCase() + '.' + this.empleado.primerApel.replace(/\s+/g, '').toLowerCase() + '.' + this.empleado.id;
    }
    if (this.empleado.pais == '0') {
      this.empleado.correo += '@cidenet.com.co';
    }
    else {
      this.empleado.correo += '@cidenet.com.us';
    }
  }

}
