import { Component, OnInit } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { NGXLogger } from 'ngx-logger';
import { Empleado } from 'src/app/Entities/Empleado';
import { EmpleadosService } from 'src/app/MicroServices/empleados.service';

@Component({
  selector: 'app-lista-empleados',
  templateUrl: './lista-empleados.component.html',
  styleUrls: ['./lista-empleados.component.css']
})
export class ListaEmpleadosComponent implements OnInit {


  constructor(private EmpleadoServices: EmpleadosService, private modalservices: NgbModal, private logger: NGXLogger) { }

  ArregloEmpleados: Empleado[] = [];
  page: number;
  IdEmpleado: number;
  modalConfirmar: NgbModalRef | undefined;

  ngOnInit(): void {
    this.ConsultaEmpleados();
  }

  ConsultaEmpleados() {
    this.EmpleadoServices.ConsultaEmpleados().subscribe({
      next: (Result) => {
        this.ArregloEmpleados = Result;
      },
      error: (Error) => {
        this.logger.error({
          message: Error.message,
          linea: "32",
          componente: "lista-empleados.component.ts",
        });
      }
    })
  }

  AbreModalElimina(modalconfirma: any, id: number) {
    this.IdEmpleado = id;
    this.modalConfirmar = this.modalservices.open(modalconfirma);
  }

  EliminaEmpleado() {
    this.EmpleadoServices.EliminaEmpleado(this.IdEmpleado).subscribe({
      next: (ResultDelete) => {
        this.ConsultaEmpleados();
        this.modalConfirmar.close();
      },
      error: (Error) => {
        this.logger.error({
          message: Error.message,
          linea: "52",
          componente: "lista-empleados.component.ts",
        });
      }
    })
  }

}
