import { NgxLoggerLevel } from "ngx-logger";

export const environment = {
  production: true,
  logLevel: NgxLoggerLevel.ERROR,
  serverLogLevel: NgxLoggerLevel.ERROR,
  EndPoint: 'http://localhost:8080/CidenetApi'
};
